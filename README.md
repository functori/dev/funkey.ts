# funkey.ts

funkey.ts is small Typescript crypto library for Tezos. It provides the
following interface:

- generate_keys
- b58enc
- b58dec
- sk_to_pk
- pk_to_pkh
- tz1_prefix
- edpk_prefix
- edsk_prefix
- size
- random
